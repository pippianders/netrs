# netrs

A simple console program to show network interface statistics.

Pull requests welcome.

# usage

It works best in an 80x24 terminal.

You'll see something that looks a bit like this:

```
interface         in       out      | in/sec   | out/sec  | in/min   | out/min  
docker0           0        0        | 0        | 0        | 0        | 0
eth0              765 MiB  678 MiB  | 41 KiB   | 591 KiB  | 2 MiB    | 29 MiB
lo                806 MiB  806 MiB  | 0        | 0        | 0        | 0
total             1 GiB    1 GiB    | 41 KiB   | 591 KiB  | 2 MiB    | 29 MiB

           ###
           ###                           #                         #
           #### #   #                    #                         #
           #### ##  #  ##   #        ##  #    ##  ########         ##
          ########  ## ######     ## ###### # ## ##########     #####
          ############ ####### ##### ######## ## ##########   #######
          ############ ####### ############################# ########
          ###########################################################
          ###########################################################
```

netr will remain running until you quit using ctrl-c.

